<?php 
define('HOST', 'localhost');
define('DAEMON_PORT', 51018);
define('WALLET_PORT', 51111);
define('ANALYTICS_HOST', HOST);
define('ANALYTICS_PORT', 15236);
define('DEBUG', true);

header('Content-Type: application/json');

if (DEBUG)
{
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}
?>
